# Net Filter flow

#### magshimim+ 2022

#### Instractor: Raz Omry

#### Created by: Ori Lev

#### Date: 20.10.2022







##### Research process:

search for "nf_hook" in linux source code.

find few intersting files.

- drivers/net/vrf.c - Virtual routing and forwarding
- include/linux/netfilter.h - defenition of the function
- net/ipv4/arp.c - data link layer
- net/ipv4/ip_forward.c - network layer, ip forwarding
- net/ipv4/ip_input.c - network layer, ip input
- net/ipv4/ip_output.c - network layer, ip output



##### The flow of NH_HOOK function:

nf_hook_slow function:

```c
int nf_hook_slow(struct sk_buff *skb, struct nf_hook_state *state,
		 const struct nf_hook_entries *e, unsigned int s)
{
	unsigned int verdict;
	int ret;

	for (; s < e->num_hook_entries; s++) {
		verdict = nf_hook_entry_hookfn(&e->hooks[s], skb, state);
		switch (verdict & NF_VERDICT_MASK) {
		case NF_ACCEPT:
			break;
		case NF_DROP:
			kfree_skb_reason(skb,
					 SKB_DROP_REASON_NETFILTER_DROP);
			ret = NF_DROP_GETERR(verdict);
			if (ret == 0)
				ret = -EPERM;
			return ret;
		case NF_QUEUE:
			ret = nf_queue(skb, state, s, verdict);
			if (ret == 1)
				continue;
			return ret;
		default:
			/* Implicit handling for NF_STOLEN, as well as any other
			 * non conventional verdicts.
			 */
			return 0;
		}
	}

	return 1;
}
```

> arg 1 - packet
>
> arg 3 - list of hooks
>
> Function flow:
>
> ​	iteratre through the hooks
>
> ​	run each hook against the packet
>
> ​	take action accordance to the hook result





Find the caller function in `include/linux/netfilter.h`

> note: I shortened the function

​		

```c
/**
 *	nf_hook - call a netfilter hook
 *
 *	Returns 1 if the hook has allowed the packet to pass.
 */
static inline int nf_hook(u_int8_t pf, unsigned int hook, struct net *net,
			  struct sock *sk, struct sk_buff *skb,
			  struct net_device *indev, struct net_device *outdev,
			  int (*okfn)(struct net *, struct sock *, struct sk_buff *))
{
	struct nf_hook_entries *hook_head = NULL;
	int ret = 1;


	rcu_read_lock();
	switch (pf) {
	case NFPROTO_IPV4:
		hook_head = rcu_dereference(net->nf.hooks_ipv4[hook]);
		break;
	case NFPROTO_IPV6:
		hook_head = rcu_dereference(net->nf.hooks_ipv6[hook]);
		break;
	case NFPROTO_ARP:
#ifdef CONFIG_NETFILTER_FAMILY_ARP
		if (WARN_ON_ONCE(hook >= ARRAY_SIZE(net->nf.hooks_arp)))
			break;
		hook_head = rcu_dereference(net->nf.hooks_arp[hook]);
		break;
    }
    
	if (hook_head) {
		struct nf_hook_state state;

		nf_hook_state_init(&state, hook, pf, indev, outdev,
				   sk, net, okfn);

		ret = nf_hook_slow(skb, &state, hook_head, 0);
	}
	rcu_read_unlock();

	return ret;
}
```



> arg 1 - flag to switch case
>
> arg 3 - skb
>
> in arg 7 - net
>
> **Impartant:** There are hook filtering process in the function, according to the protocol (arg 1)
>
> After the filtering process, call to nf_hook_slow
>
> In the beggining of the function `ret = 1;`
>
> ret change to the `nf_hook_slow` result

NF_HOOK Function:

```c
static inline int
NF_HOOK(uint8_t pf, unsigned int hook, struct net *net, struct sock *sk, struct sk_buff *skb,
	struct net_device *in, struct net_device *out,
	int (*okfn)(struct net *, struct sock *, struct sk_buff *))
{
	int ret = nf_hook(pf, hook, net, sk, skb, in, out, okfn);
	if (ret == 1)
		ret = okfn(net, sk, skb);
	return ret;
}
```

>  call to `nf_hook` function
>
> if `ret = 1`
>
> call to the okon (the function parameter)



#### Now, let`s dive in into the IP functions

```c
net/ipv4/ip_forward.c:
  163: 	return NF_HOOK(NFPROTO_IPV4, NF_INET_FORWARD,

net/ipv4/ip_input.c:
  254: 	return NF_HOOK(NFPROTO_IPV4, NF_INET_LOCAL_IN,
  564: 	return NF_HOOK(NFPROTO_IPV4, NF_INET_PRE_ROUTING,

net/ipv4/ip_output.c:
  115: 	return nf_hook(NFPROTO_IPV4, NF_INET_LOCAL_OUT,
  394: 				NF_HOOK(NFPROTO_IPV4, NF_INET_POST_ROUTING,
  410: 			NF_HOOK(NFPROTO_IPV4, NF_INET_POST_ROUTING,
```



- forward function:

  

  ```c
  int ip_forward(struct sk_buff *skb)
  {
  	u32 mtu;
  	struct iphdr *iph;	/* Our header */
  	struct rtable *rt;	/* Route we use */
  	struct ip_options *opt	= &(IPCB(skb)->opt);
  	struct net *net;
  	SKB_DR(reason);
  
  	/* that should never happen */
  	if (skb->pkt_type != PACKET_HOST)
  		goto drop;
  
  	if (unlikely(skb->sk))
  		goto drop;
  
  	if (skb_warn_if_lro(skb))
  		goto drop;
  
  	if (!xfrm4_policy_check(NULL, XFRM_POLICY_FWD, skb)) {
  		SKB_DR_SET(reason, XFRM_POLICY);
  		goto drop;
  	}
  
  	if (IPCB(skb)->opt.router_alert && ip_call_ra_chain(skb))
  		return NET_RX_SUCCESS;
  
  	skb_forward_csum(skb);
  	net = dev_net(skb->dev);
  
  	/*
  	 *	According to the RFC, we must first decrease the TTL field. If
  	 *	that reaches zero, we must reply an ICMP control message telling
  	 *	that the packet's lifetime expired.
  	 */
  	if (ip_hdr(skb)->ttl <= 1)
  		goto too_many_hops;
  
  	if (!xfrm4_route_forward(skb)) {
  		SKB_DR_SET(reason, XFRM_POLICY);
  		goto drop;
  	}
  
  	rt = skb_rtable(skb);
  
  	if (opt->is_strictroute && rt->rt_uses_gateway)
  		goto sr_failed;
  
  	IPCB(skb)->flags |= IPSKB_FORWARDED;
  	mtu = ip_dst_mtu_maybe_forward(&rt->dst, true);
  	if (ip_exceeds_mtu(skb, mtu)) {
  		IP_INC_STATS(net, IPSTATS_MIB_FRAGFAILS);
  		icmp_send(skb, ICMP_DEST_UNREACH, ICMP_FRAG_NEEDED,
  			  htonl(mtu));
  		SKB_DR_SET(reason, PKT_TOO_BIG);
  		goto drop;
  	}
  
  	/* We are about to mangle packet. Copy it! */
  	if (skb_cow(skb, LL_RESERVED_SPACE(rt->dst.dev)+rt->dst.header_len))
  		goto drop;
  	iph = ip_hdr(skb);
  
  	/* Decrease ttl after skb cow done */
  	ip_decrease_ttl(iph);
  
  	/*
  	 *	We now generate an ICMP HOST REDIRECT giving the route
  	 *	we calculated.
  	 */
  	if (IPCB(skb)->flags & IPSKB_DOREDIRECT && !opt->srr &&
  	    !skb_sec_path(skb))
  		ip_rt_send_redirect(skb);
  
  	if (READ_ONCE(net->ipv4.sysctl_ip_fwd_update_priority))
  		skb->priority = rt_tos2priority(iph->tos);
  
  	return NF_HOOK(NFPROTO_IPV4, NF_INET_FORWARD,
  		       net, NULL, skb, skb->dev, rt->dst.dev,
  		       ip_forward_finish);
  
  sr_failed:
  	/*
  	 *	Strict routing permits no gatewaying
  	 */
  	 icmp_send(skb, ICMP_DEST_UNREACH, ICMP_SR_FAILED, 0);
  	 goto drop;
  
  too_many_hops:
  	/* Tell the sender its packet died... */
  	__IP_INC_STATS(net, IPSTATS_MIB_INHDRERRORS);
  	icmp_send(skb, ICMP_TIME_EXCEEDED, ICMP_EXC_TTL, 0);
  	SKB_DR_SET(reason, IP_INHDR);
  drop:
  	kfree_skb_reason(skb, reason);
  	return NET_RX_DROP;
  }
  ```

  > **Important: ** `skb` argument include all we need, hooks list, skb, flag type, etc
  >
  > function flow:
  >
  > some logic of drop/accept
  >
  > return the hook result

- input functions:

  ```c
  /*
   * 	Deliver IP Packets to the higher protocol layers.
   */
  int ip_local_deliver(struct sk_buff *skb)
  {
  	/*
  	 *	Reassemble IP fragments.
  	 */
  	struct net *net = dev_net(skb->dev);
  
  	if (ip_is_fragment(ip_hdr(skb))) {
  		if (ip_defrag(net, skb, IP_DEFRAG_LOCAL_DELIVER))
  			return 0;
  	}
  
  	return NF_HOOK(NFPROTO_IPV4, NF_INET_LOCAL_IN,
  		       net, NULL, skb, skb->dev, NULL,
  		       ip_local_deliver_finish);
  }
  ```

  > soem logic of getting the whole packet
  >
  > [credint](https://people.computing.clemson.edu/~westall/853/notes/iprecv2.pdf)
  >
  > return the hook result

  ```c
  /*
   * IP receive entry point
   */
  int ip_rcv(struct sk_buff *skb, struct net_device *dev, struct packet_type *pt,
  	   struct net_device *orig_dev)
  {
  	struct net *net = dev_net(dev);
  
  	skb = ip_rcv_core(skb, net);
  	if (skb == NULL)
  		return NET_RX_DROP;
  
  	return NF_HOOK(NFPROTO_IPV4, NF_INET_PRE_ROUTING,
  		       net, NULL, skb, dev, NULL,
  		       ip_rcv_finish);
  }
  ```

  > The main input function
  >
  > get the skb (the struct with all the data + hooks)
  >
  > return the hook result

- output functions:

  > Note: two functions

  ```c
  int __ip_local_out(struct net *net, struct sock *sk, struct sk_buff *skb)
  {
  	struct iphdr *iph = ip_hdr(skb);
  
  	iph->tot_len = htons(skb->len);
  	ip_send_check(iph);
  
  	/* if egress device is enslaved to an L3 master device pass the
  	 * skb to its handler for processing
  	 */
  	skb = l3mdev_ip_out(sk, skb);
  	if (unlikely(!skb))
  		return 0;
  
  	skb->protocol = htons(ETH_P_IP);
  
  	return nf_hook(NFPROTO_IPV4, NF_INET_LOCAL_OUT,
  		       net, sk, skb, NULL, skb_dst(skb)->dev,
  		       dst_output);
  }
  
  int ip_local_out(struct net *net, struct sock *sk, struct sk_buff *skb)
  {
  	int err;
  
  	err = __ip_local_out(net, sk, skb);
  	if (likely(err == 1))
  		err = dst_output(net, sk, skb);
  
  	return err;
  }
  ```

  > function one do some logic and return the hook result
  >
  > **Important:** call to `nf_hook` not `NF_HOOK`
  >
  > 
  >
  > function two - call function one and check if `err == 1`  (like `NF_HOOK` function)

  ```c
  int ip_mc_output(struct net *net, struct sock *sk, struct sk_buff *skb)
  {
  	struct rtable *rt = skb_rtable(skb);
  	struct net_device *dev = rt->dst.dev;
  
  	/*
  	 *	If the indicated interface is up and running, send the packet.
  	 */
  	IP_UPD_PO_STATS(net, IPSTATS_MIB_OUT, skb->len);
  
  	skb->dev = dev;
  	skb->protocol = htons(ETH_P_IP);
  
  	/*
  	 *	Multicasts are looped back for other local users
  	 */
  
  	if (rt->rt_flags&RTCF_MULTICAST) {
  		if (sk_mc_loop(sk)
  #ifdef CONFIG_IP_MROUTE
  		/* Small optimization: do not loopback not local frames,
  		   which returned after forwarding; they will be  dropped
  		   by ip_mr_input in any case.
  		   Note, that local frames are looped back to be delivered
  		   to local recipients.
  
  		   This check is duplicated in ip_mr_input at the moment.
  		 */
  		    &&
  		    ((rt->rt_flags & RTCF_LOCAL) ||
  		     !(IPCB(skb)->flags & IPSKB_FORWARDED))
  #endif
  		   ) {
  			struct sk_buff *newskb = skb_clone(skb, GFP_ATOMIC);
  			if (newskb)
  				NF_HOOK(NFPROTO_IPV4, NF_INET_POST_ROUTING,
  					net, sk, newskb, NULL, newskb->dev,
  					ip_mc_finish_output);
  		}
  
  		/* Multicasts with ttl 0 must not go beyond the host */
  
  		if (ip_hdr(skb)->ttl == 0) {
  			kfree_skb(skb);
  			return 0;
  		}
  	}
  
  	if (rt->rt_flags&RTCF_BROADCAST) {
  		struct sk_buff *newskb = skb_clone(skb, GFP_ATOMIC);
  		if (newskb)
  			NF_HOOK(NFPROTO_IPV4, NF_INET_POST_ROUTING,
  				net, sk, newskb, NULL, newskb->dev,
  				ip_mc_finish_output);
  	}
  
  	return NF_HOOK_COND(NFPROTO_IPV4, NF_INET_POST_ROUTING,
  			    net, sk, skb, NULL, skb->dev,
  			    ip_finish_output,
  			    !(IPCB(skb)->flags & IPSKB_REROUTED));
  }
  ```

> Running the hook twice, without save the return value